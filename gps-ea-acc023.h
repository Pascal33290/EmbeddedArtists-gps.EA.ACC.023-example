/* Gestion carte GPS au format XBEE
 * Fournisseur : Embedded Artists - Réf : EA-ACC-023
 * 		https://www.embeddedartists.com/products/acc/acc_gps.php
 * Utilise Chipset MT3339 (Mediatek Labs)
 * Décode et affiche trame $GPRMC ou $GPGGA - affiche les autres trames non décodées
 * Caractères reçus sous interruption (UART1-RX)
 *
 * V2 14/3/2018 - ajout décodage trame GGA
 *
 * Exemple d'utilisation class GpsXbee (IRQ compatible Eventqueue) :
@code
	#include "gps.h"
	#include "mbed.h"
	#include "mbed_events.h"
	#include <stdio.h>

	using namespace gpsxbee_MT3339;
	static GpsXbee gpsxbee;
	extern RawSerial *gps_uart; // GPS connecté à UART1

	EventQueue eventQueue(3 * EVENTS_EVENT_SIZE);
	Thread t(osPriorityNormal);

	void post_to_queue_gps();

	void queue_gps(){
		gpsxbee.lecture();

		if (gpsxbee.gpsTrame == gpsxbee.TRAME_OK) { // affichage données GPS quand trame complète reçue
			gpsxbee.gpsTrame = gpsxbee.TRAME_WAIT;
			printf(" %s\r\n",gpsxbee.gpsData);
		}
		gps_uart -> attach(post_to_queue_gps, RawSerial::RxIrq); // relance IRQ réception GPS
	}

	void post_to_queue_gps(){
		gpsxbee.~GpsXbee(); // bloque IRQ réception GPS
		eventQueue.call(queue_gps);

	int main() {
  	  RawSerial(USBTX, USBRX,115200); // pour changer vitesse "printf" sur port Terminal USB

	  t.start(callback(&eventQueue, &EventQueue::dispatch_forever));
	  gps_uart ->attach(post_to_queue_gps, RawSerial::RxIrq);  // lecture GPS sous IRQ réception
	  return(0);
	}
@endcode

 *	Résultat  = Affichage sur terminal PC (115200 bauds) :
 *	Trame type RMC ou GGA décodée, autres trames affichées sans décodage :

	NMEARMC : 14/03/18 :10H43mn27s --- Latitude = 44.957655N  --- Longitude = -0.666488W
*/

#ifndef GPSXBEE_H_
#define GPSXBEE_H_

namespace gpsxbee_MT3339 {

class GpsXbee {

public:
	GpsXbee();
    virtual ~GpsXbee();
    void InitRMC(void);
    void InitGGA(void);

    void parseRMC(char* data, int dataLen); // analyse trame GPS
    int  parseData(char* data, int len);
    void lecture(); // Remplissage gpsBuf
    void parseGGA(char* data, int dataLen);

    // Conversion format ddmm.mmmm en mode degrés
    double  getLatitudeAsDegrees(double l,char ns);
    double  getLongitudeAsDegrees(double l,char ns);

    enum NmeaSentenceValue { // type de protocole de message reçu
    	NMEA_INVALID   	= 0,
		NmeaGga  		= 0x01,
		NmeaGsa 		= 0x02,
		NmeaGsv 		= 0x04,
		NmeaRmc 		= 0x08,
		NmeaVtg  		= 0x10,
    };
    NmeaSentenceValue NmeaSentence;

    enum gpsTrameValue {
		TRAME_WAIT = 0, 	// Trame lue, attente nouvelle trame
		TRAME_IN   = 1,   	// Reception Trame en cours
		TRAME_OK   = 2,   	// Trame complète prête à lire
		TRAME_ERR  = -1, 	// Erreur Trame
    };
    gpsTrameValue  gpsTrame;

    // Message reçu au format $GPRMC
	int rmc_hours;			/** UTC time - hours */
	int rmc_minutes;		/** UTC time - minutes */
	int rmc_seconds;		/** UTC time - seconds */
	int rmc_milliseconds;	/** UTC time - milliseconds */
	char rmc_warning;		/** A = données valides - V = invalides */
	double rmc_latitude; 	/** The latitude in ddmm.mmmm format (d = degrees, m = minutes) */
	double rmc_longitude;	/** The longitude in dddmm.mmmm format */
	char rmc_nsIndicator;	/** North / South indicator */
	char rmc_ewIndicator;	/** East / West indicator */
	double rmc_speedKnots;	/** speed in Knots */
	int rmc_jours ;			/** date jjmmaa */
	int rmc_mois ;
    int rmc_ans ;

    // Message reçu au format $GPGGA
	int gga_hours;
	int gga_minutes;
	int gga_seconds;
	int gga_milliseconds;
	double gga_latitude;
	double gga_longitude;
	char gga_nsIndicator;
	char gga_ewIndicator;
	int gga_fix; 			//  0 = Fix not available     1 = GPS fix   2 = Differential GPS fix
	int gga_satellites;		/** Number of used satellites */
	double gga_hdop;  		/** Horizontal Dilution of Precision */
	double gga_altitude;	/** antenna altitude above/below mean sea-level */
	double gga_geoidal;		/** geoidal separation */

    char gpsData[200];		/** buffer pour affichage */

private :
    #define MTK3339_BUF_SZ 200		// Taille gpsBuf
    char gpsBuf[MTK3339_BUF_SZ];  	// buffer de réception trame GPS

    enum gpsStateValue {
		StateStart = 0x30,
		StateData = 0x31,
    };
    gpsStateValue gpsState; 		// état réception

    int  gpsBufPos;  				// position index lecture buffer

}; // end class
}  // end namespace
#endif /* GPSXBEE_H_ */
